﻿using UnityEngine;
using System.Net;

using System.Collections;

public class ScratchController : MonoBehaviour
{

	public float speed = 7000f;
	
	Quaternion rotation;

	public GameObject floor;

	public Vector3 velocity;

	// Use this for initialization
	void Start ()
	{
		ScratchExtensionServer server = new ScratchExtensionServer("http://127.0.0.1:50209");

		server.Add(Forward, "forward");
        
		server.Add(Back, "back");

		server.Add(Left, "left");

		server.Add(Right, "right");

        server.Run();

		rotation = this.transform.rotation;
		
		Physics.IgnoreCollision(gameObject.GetComponent<Collider>(), floor.GetComponent<Collider>());
   }



    public void Forward(HttpListenerRequest request)
    {
		velocity = speed * Vector3.forward;						
	}

	public void Back(HttpListenerRequest request)
	{
		velocity = speed * Vector3.back;
	}

	public void Left(HttpListenerRequest request)
	{
		rotation = Quaternion.Euler( rotation.eulerAngles.x,
									 					  0, 
									 rotation.eulerAngles.z);

		velocity = speed * Vector3.left;
	}

	public void Right(HttpListenerRequest request)
	{

		rotation = Quaternion.Euler( rotation.eulerAngles.x,
									 					180,
									 rotation.eulerAngles.z );
		
		velocity = speed * Vector3.right;	
	}
	
	// Update is called once per frame
    void Update()
	{
		GetComponent<Rigidbody>().velocity = Vector3.zero;

		GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

		GetComponent<Rigidbody>().AddForce (velocity);

		this.transform.rotation = rotation;

		
		StartCoroutine (WaitForSeconds (5));
	} 

	void OnCollisionEnter() 
	{

		GetComponent<AudioSource>().Play ();
	}

	IEnumerator WaitForSeconds(int seconds)
	{
		yield return new WaitForSeconds(seconds);
	}
}
