﻿using System.Net;
using System;
using System.Threading;
using System.Text;
using System.Collections.Generic;

public class ScratchExtensionServer
{
	private readonly HttpListener listener = new HttpListener();

	private readonly Dictionary<string, Action<HttpListenerRequest>> responderMethod  = new Dictionary<string, Action<HttpListenerRequest>>();

	private readonly string baseURI;

	public ScratchExtensionServer(string URI)
	{
		baseURI = URI;
	}

	public void Add(Action<HttpListenerRequest> method, string prefix)
	{
		listener.Prefixes.Add(baseURI + "/" + prefix + "/");

		responderMethod.Add("/" + prefix, method);
	}

	public void Run()
	{
		ThreadPool.QueueUserWorkItem((o) =>
		{
			try
			{
				listener.Start ();

				while (listener.IsListening)
				{
					ThreadPool.QueueUserWorkItem((c) =>
					{
						var context = c as HttpListenerContext;

						try
						{
							responderMethod[context.Request.RawUrl](context.Request);					
						}
						catch { } // suppress any exceptions
						
						finally
						{
							// always close the stream
							context.Response.OutputStream.Close();
						}
					}, listener.GetContext());
				}
			}
			catch { } // suppress any exceptions
		});

		
	}

	public void Stop()
	{
		listener.Stop();
		listener.Close();
	}
}