﻿using metaio;
using UnityEngine;
using System.Collections;

public class CameraCalibration : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    MetaioSDKUnity.setCameraParametersFromFile( Application.dataPath +
                                                    "/StreamingAssets/CalibrationResults.xml"
																	 //"/StreamingAssets/Calibration-Philips.xml"
                                                    );
	}
}