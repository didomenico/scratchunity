﻿using UnityEngine;
using System.Collections;

public class DisableAR : MonoBehaviour
{
	public metaioSDK metaioSDK;

	public metaioCallback metaioCallback;

	public metaioCamera metaioCamera;

	public metaioDeviceCamera metaioDeviceCamera;

	public metaioTracker metaioTracker;

	// Use this for initialization
	void Start ()
	{			
		metaioSDK.enabled = false;

		metaioCallback.enabled = false;

		metaioCamera.enabled = false;

		metaioDeviceCamera.enabled = false;

		metaioTracker.enabled = false;

		metaioCamera.transform.position = new Vector3 (0f, 408.15f, -617.9f);

		metaioCamera.transform.rotation = Quaternion.Euler(33.4f, 0f, 0f);
	}	
}
