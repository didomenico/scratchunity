﻿using metaio;
using System.Collections.Generic;
using UnityEngine;

public class CameraResolution : MonoBehaviour
{

    public int width;

    public int height;

    // Use this for initialization
    void Start()
    {
        List<MetaioCamera> cameras = MetaioSDKUnity.getCameraList();

        foreach (MetaioCamera camera in cameras)
        {
            if (camera.facing == MetaioCamera.FACE_UNDEFINED)
            {
                camera.resolution = new Vector2di(width, height);

                camera.downsample = 1;

                MetaioSDKUnity.startCamera(camera);
                
                break;
            }
        }
    }
}
