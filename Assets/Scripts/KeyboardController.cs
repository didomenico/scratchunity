﻿using UnityEngine;
using System.Collections;

public class KeyboardController : MonoBehaviour
{
	public float speed = 5f;

	public GameObject floor;

	// Use this for initialization
	void Start ()
	{
			Physics.IgnoreCollision(gameObject.GetComponent<Collider>(), floor.GetComponent<Collider>());
	}

	// Update is called once per frame
	void Update ()
	{
		GetComponent<Rigidbody>().velocity = Vector3.zero;
		
		GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
		
		float vertical = Input.GetAxis("Vertical");
		
		GetComponent<Rigidbody>().velocity = vertical * speed * Vector3.back * Time.deltaTime;
		
		float horizontal = Input.GetAxis("Horizontal");

		GetComponent<Rigidbody>().velocity += horizontal * speed * Vector3.right * Time.deltaTime;
	}
}
