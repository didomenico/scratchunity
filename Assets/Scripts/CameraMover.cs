﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

	public Transform player;

	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.LookAt (player);

		if (Input.GetButton ("LeftShoulder")) 
		{
			transform.RotateAround(player.position, Vector3.up, 60 * Time.deltaTime);

		}

		else if (Input.GetButton ("RightShoulder"))
		{
			transform.RotateAround(player.position, Vector3.up, -60 * Time.deltaTime);
		}
	}
}
